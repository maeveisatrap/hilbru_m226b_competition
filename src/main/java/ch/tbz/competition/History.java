package ch.tbz.competition;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class History {
    File file;
    LinkedHashMap<String, String> bands = new LinkedHashMap<>();


    public History() {
        readFile();
    }

    private void readFile() {
        try {
            file = new File("history.json");
            Scanner s = new Scanner(file);
            String scoreString = "";
            while (s.hasNextLine()) {
                scoreString += s.nextLine();
            }
            ObjectMapper mapper = new ObjectMapper();
            // parse String to Map
            bands = mapper.readValue(scoreString.toString(), LinkedHashMap.class);
            s.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * The following gets used to show the currently active competition's results.
     */
    public void printResults() {
        for (Map.Entry<String, String> entry : bands.entrySet()) {
            System.out.println("Band: " + entry.getKey() + "   The result was: " + entry.getValue());
        }
    }

    public void addNewResults(String winners, String results) {
        this.bands.put(winners, results);
    }

    /**
     * This is used to save the competition's results to a file called "history.json"
     */
    public void saveResults() {
        try {
            FileWriter fileWriter = new FileWriter("history.json");
            String writeTo = "{";

            for (Map.Entry<String, String> entry : bands.entrySet()) {
                writeTo += "\"" + entry.getKey() + "\":\"" + entry.getValue() + "\",";
            }
            writeTo = writeTo.substring(0, writeTo.length() - 1);
            writeTo += "}";
            fileWriter.write(writeTo);
            fileWriter.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
