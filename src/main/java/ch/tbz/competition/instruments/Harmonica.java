package ch.tbz.competition.instruments;

// Example of Inheritance
public class Harmonica extends  Instrument {

    Double breathCapacity;

    // Required when parent has constructor
    public Harmonica(Double successFactor, Double breathCapacity){
        // Calls parent constructor
        super(successFactor);


        this.breathCapacity = breathCapacity;
    }


    // Must be defined with @Override
    @Override
    public Double calcSuccessRate() {
        return getSuccessRate() * getSuccessFactor() + breathCapacity;
    }

    // Example of standard Override method
    @Override
    public Double getSuccessFactor(){
        return  breathCapacity;
    }
}

