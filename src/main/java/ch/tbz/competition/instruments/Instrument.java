package ch.tbz.competition.instruments;

import lombok.Getter;

//Example of an Abstract Class
public abstract class Instrument {

    @Getter
    private Double successRate = 1.0;
    @Getter
    private Double successFactor;
//Example of the Abstract Method (must be defined in delegated object)
    public abstract Double calcSuccessRate();


//Example of a Constructor
    public Instrument(Double successFactor) {
        this.successFactor = successFactor;
    }


}
