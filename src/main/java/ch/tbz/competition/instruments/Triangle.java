package ch.tbz.competition.instruments;


// Example of Inheritance
public class Triangle extends  Instrument {

    Double percussionPerMinute;

    // Required when parent has constructor
    public Triangle(Double successFactor, Double percussionPerMinute){
        // Calls parent constructor
        super(successFactor);


        this.percussionPerMinute = percussionPerMinute;
    }


    // Must be defined with @Override
    @Override
    public Double calcSuccessRate() {
        return getSuccessRate() * getSuccessFactor() + this.percussionPerMinute;
    }

    // Example of standard Override method
    @Override
    public Double getSuccessFactor(){
        return  this.percussionPerMinute;
    }
}
