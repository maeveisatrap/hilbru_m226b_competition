package ch.tbz.competition.instruments;

// Example of Inheritance
public class Mayonnaise extends  Instrument {

    Double beautyOfSound;

    // Required when parent has constructor
    public Mayonnaise(Double successFactor, Double beautyOfSound){
        // Calls parent constructor
        super(successFactor);


        this.beautyOfSound = beautyOfSound;
    }


    // Must be defined with @Override
    @Override
    public Double calcSuccessRate() {
        return getSuccessRate() * getSuccessFactor() + beautyOfSound;
    }

    // Example of standard Override method
    @Override
    public Double getSuccessFactor(){
        return this.beautyOfSound;
    }
}


