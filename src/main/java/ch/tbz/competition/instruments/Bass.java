package ch.tbz.competition.instruments;
// Example of Inheritance
public class Bass extends  Instrument {

    Double backgroundNoise;

    // Required when parent has constructor
    public Bass(Double successFactor, Double backgroundNoise){
        // Calls parent constructor
        super(successFactor);


        this.backgroundNoise = backgroundNoise;
    }


    // Must be defined with @Override
    @Override
    public Double calcSuccessRate() {
        return getSuccessRate() * getSuccessFactor() + backgroundNoise;
    }

    // Example of standard Override method
    @Override
    public Double getSuccessFactor(){
        return  backgroundNoise;
    }
}

