package ch.tbz.competition.band;

import ch.tbz.competition.instruments.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Band implements BandInterface {
    @Getter
    // Aggregation
    List<Instrument> bandsInstruments = new ArrayList<>();

    // Composition
    final Person leadSinger;

    // Association
    Instrument leadInstrument;


    String bandName;


    public Band(String bandName, String leadSingerName){
        this.bandName = bandName;

        // Add list elements
        bandsInstruments.add(new Triangle(10.0,85.0));
        bandsInstruments.add(new Mayonnaise(50.0, 99.0));
        bandsInstruments.add(new Harmonica(20.0, 95.0));
        bandsInstruments.add(new Bass(25.0, 80.0));

        // Must be defined in Constructor because final
        this.leadSinger = new Person(leadSingerName);

        // Association example
   /*     this.leadInstrument = new Triangle(4.0,70.0);
        this.leadInstrument = new Mayonnaise(4.0, 99.0);
        this.leadInstrument = new Harmonica(4.0, 80.0);
     */   this.leadInstrument = new Bass(4.0, 70.0);
    }


    public Double getFinalScore(Double luck){
        Double finalScore = 0.0;

        for (Instrument bandsInstrument : bandsInstruments) {
          finalScore +=  bandsInstrument.calcSuccessRate();
        }

        for (Instrument inst : bandsInstruments) {
          finalScore +=  inst.calcSuccessRate();
        }
        return finalScore * luck;
    }

    /**
     * This returns the band name
     * @return String bandName
     */
    public  String getBandName(){
        return  this.bandName;
    }

    @Override
    public void play() {
        System.out.println("it sounds nice!");
    }
}
