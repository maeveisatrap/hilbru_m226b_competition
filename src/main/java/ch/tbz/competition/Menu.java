package ch.tbz.competition;

import ch.tbz.competition.band.Band;
import ch.tbz.competition.instruments.Bass;
import ch.tbz.competition.instruments.Harmonica;
import ch.tbz.competition.instruments.Mayonnaise;
import ch.tbz.competition.instruments.Triangle;

import java.util.*;

/**
 * This is the menu class it is the entry point for the program
 */
public class Menu {

    private boolean exit = false;
    private History history = new History();
    private List<Band> allBands = new ArrayList<Band>();
    private Integer min = 1;
    private Integer max = 20;
    private Random r = new Random();


    public void mainMenu() {
        Scanner s = new Scanner(System.in);

        try {
            while (!exit) {
                System.out.println("********************************************\nWelcome to the competition please choose from:\n1. Add new Band \n2. Select a winner \n3. Add an instrument \n4. List of Bands \n5. Quit");
                int choice = s.nextInt();
                for (Map.Entry<String, String> entry : history.bands.entrySet()) {
                    Band b = new Band(entry.getKey(), "bob");
                    this.allBands.add(b);
                }

                /**
                 * Here is a Switch case which goes into another Switch case with both of which the User will make choices.
                 */
                switch (choice) {
                    case 1:
                        System.out.println("how should the band be called?");
                        String bandName = "";
                        String leadSinger = "";
                        s.nextLine();
                        bandName = s.nextLine();
                        System.out.println("and what is the lead singers name?");
                        leadSinger = s.nextLine();
                        Band b = new Band(bandName, leadSinger);
                        this.allBands.add(b);
                        break;
                    case 2:
                        Double winnerScore = 0.0;
                        Band winnerBand = this.allBands.get(0);
                        System.out.println(this.allBands.size());
                        for (Band band : this.allBands
                        ) {
                            // max = 10, min 1; 10-1 + 1 + 1
                            Integer luck = r.nextInt(max - min + 1) + min;
                            if (band.getFinalScore(luck.doubleValue()) > winnerScore) {
                                winnerScore = band.getFinalScore(luck.doubleValue());
                                winnerBand = band;
                            }
                            history.addNewResults(band.getBandName(), band.getFinalScore(luck.doubleValue()).toString());
                        }
                        System.out.println("the winner is: " + winnerBand.getBandName() + "\n with the score " + winnerScore);
                        break;
                    case 3:
                        System.out.println("3");
                        System.out.println("what instrument?\n1. Triangle \n2. Harmonica \n3. Bass \n4. Mayonnaise");
                        int newChoice = s.nextInt();
                        switch (newChoice) {
                            case 1:
                                for (Band band : allBands
                                ) {
                                    band.getBandsInstruments().add(new Triangle(5.0, 10.0));
                                }
                                break;
                            case 2:
                                for (Band band : allBands
                                ) {
                                    band.getBandsInstruments().add(new Harmonica(5.0, 20.0));
                                }
                                break;
                            case 3:
                                for (Band band : allBands
                                ) {
                                    band.getBandsInstruments().add(new Bass(5.0, 10.0));
                                }
                                break;
                            case 4:
                                for (Band band : allBands
                                ) {
                                    band.getBandsInstruments().add(new Mayonnaise(5.0, 40.0));
                                }
                                break;

                        }

                        break;
                    case 4:
                        history.printResults();
                        break;
                    case 5:
                        System.out.println("Goodbye");
                        exit = true;
                        break;
                    default:
                        System.out.println("wrong selection");
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            history.saveResults();
            s.close();
        }
    }
}
